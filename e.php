<?php

function e($str, $key){
    $enc = function (...$a) use (&$enc){
        switch($a[0]){
            default : 
                return $enc(8, $a[0], $a[1]);
            case 1 : // Master Key
                $mk = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-!@$%;&*( ),.|[]{}:`^~/<>?_+=";
                return isset($a[1]) ? substr($mk,0, strlen($mk) - 10) : $mk;
            case 2 : // Original Array
                return str_split($enc(1));
            case 3 : // First Substitution Table
                $permutiveArray = $enc(2);
                $subtituteTable = [];
                foreach ($enc(2) as $k => $v) {
                    if ($k != 0) {
                        $permutiveValue = array_shift($permutiveArray);
                        $permutiveArray[] = $permutiveValue;
                    }
                    $subtituteTable[] = $permutiveArray;
                }
                return $subtituteTable;
            case 4 : // First Encipherment
                return !in_array($a[2], $enc(2)) || !in_array($a[1], $enc(2))  ? $a[1] : $enc(3)[array_search($a[2], $enc(2))][array_search($a[1], $enc(2))];
            case 5 : // First Dicipherment
                return  !in_array($a[2], $enc(2)) || !in_array($a[1], $enc(2)) ? $a[1] : $enc(2)[array_search($a[1], $enc(3)[array_search($a[2], $enc(2))])];
            case 6 : //First encipher and decipher converssion
                $flag = 0;
                $data = [
                    'pt' => $a[1],
                    'key' => $a[2]
                ];
                if (strpos($a[1], '^|p`t|^')) {
                    $data['pt'] = explode('^|p`t|^', $a[1])[0];
                }
                $str_s = mb_strlen($data['pt']);
                $key_s = mb_strlen($data['key']);
                if ($str_s > $key_s) {
                    $ni = $str_s / $key_s;
                    $pre_permutive = $data['key'];
                    $flag = 1;
                } else if ($str_s < $key_s) {
                    $ni = $key_s / $str_s;
                    $pre_permutive = $data['pt'] . '^|x`t|^';
                    $flag = 2;
                }
                $substitute = '';
                if (isset($ni)) {
                    for ($i = 0; $i < $ni; $i++) {
                        $substitute .= $pre_permutive;
                    }
                }
                if ($flag == 1) {
                    $data['key'] = substr($substitute, 0, $str_s);
                } else if ($flag == 2) {
                    $data['pt'] = substr($substitute, 0, $key_s);;
                }
                $txt = '';
                if (!strpos($a[1], '^|p`t|^')) {
                    foreach (str_split($data['pt']) as $k => $v) {
                        $txt .= $enc(4,$data['pt'][$k], $data['key'][$k]);
                    }
                    return $txt . '^|p`t|^';
                }
                foreach (str_split($data['pt']) as $k => $v) {
                    $txt .= $enc(5, $data['pt'][$k], $data['key'][$k]);
                }
                return explode('^|x`t|^',$txt)[0];
            case 7 : 
                return array_chunk($a[1], $a[2]);
            case 8 :
                $squizedKey  = [];
                $split = str_split($a[2] . $enc(1,1));
                foreach($split as $v){
                    if(array_search($v, $squizedKey) === false){
                        $squizedKey[] = $v;
                    }
                }
                return $enc(7, $squizedKey, 9);
        }
    };
    return $enc($str, $key);
}
if(isset($_REQUEST['e'])){
$nl = "
";  
    echo str_replace($nl, '', str_replace('  ', '', file_get_contents('e.js')));
    // echo file_get_contents('e.js');
}
?>