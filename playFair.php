<?php
$s = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-!@$%;&*( ),.|[]{}:';
function getSubtitute($key){
    global $s;
    $squizedKey  = [];
    $split = str_split($key . $s);
    foreach($split as $v){
        if(array_search($v, $squizedKey) === false){
            $squizedKey[] = $v;
        }
    }
    return array_chunk($squizedKey, 9);
}
function getOperated($chs, $sub, $push){
    $holder = [];
    $product = [];
    foreach($sub as $key=>$row){
        if(in_array($chs[0],$row)){
            $holder[0] = [$key, array_search($chs[0], $row)];
        }
    }
    foreach($sub as $key=>$row){
        if(in_array($chs[1],$row)){
            $holder[1] = [$key, array_search($chs[1], $row)];
        }
    }
    $row = $holder[0][0] == $holder[1][0];
    $col = $holder[0][1] == $holder[1][1];
    if($push){
        if($row){
            $product = [$holder[0][1] == 8 ? $sub[$holder[0][0]][0] : $sub[$holder[0][0]][$holder[0][1] + 1], $holder[1][1] == 8 ? $sub[$holder[0][0]][0] : $sub[$holder[0][0]][$holder[1][1] + 1]];
        }elseif($col){
            $product = [$sub[$holder[0][0] != 8 ? ($holder[0][0] + 1) : 0][$holder[0][1]], $sub[$holder[1][0] != 8 ? ($holder[1][0] + 1) : 0][$holder[1][1]]];
        }else{
            $product = [$sub[$holder[1][0]][$holder[0][1]], $sub[$holder[0][0]][$holder[1][1]]];
        }
    }else{
        if($row){
            $product = [$holder[0][1] == 0 ? $sub[$holder[0][0]][8] : $sub[$holder[0][0]][$holder[0][1] - 1], $holder[1][1] == 0 ? $sub[$holder[0][0]][8] : $sub[$holder[0][0]][$holder[1][1] - 1]];
        }elseif($col){
            $product = [$sub[$holder[0][0] == 0 ? 8 : ($holder[0][0] - 1)][$holder[0][1]], $sub[$holder[1][0] == 0 ? 8 : ($holder[1][0] - 1)][$holder[1][1]]];
        }else{
            $product = [$sub[$holder[1][0]][$holder[0][1]], $sub[$holder[0][0]][$holder[1][1]]];
        }
    }
    return $product;
}
$pt = 'Emmet is great for that. With it installed in the code editor you are using, you can type "lorem" and then tab and it will expand into a paragraph of Lorem Ipsum placeholder text. But it can do more! You can control how much you get, place it within [HTML] structure as it expands, and get different bits of it in repeated elements.';
$keyy = 'A Quick Brown Fox Jumps Over The Lazy Dog';
echo $pt . "<br>" . $keyy;
echo "<br>" . $enc = e($pt, $keyy);
echo "<br><br>" . e($enc, $keyy);



function e($st, $k){
    global $s;
    $op = true;
    if(strpos($st, '^|p`t|^')){
        $st = explode('^|p`t|^', $st)[0];
        $op = false;
    }
    $c = '';
    if(strlen($st)%2){
        $st .= '|^f^|';
    }
    $st = array_chunk(str_split($st), 2);
    foreach($st as $ch){
        if(in_array($ch[0], str_split($s)) === false || in_array($ch[1], str_split($s)) === false){
            $c .= implode('', $ch);
        }else{
            $c .= implode('', getOperated($ch, getSubtitute($k), $op));
        }
    }
    return str_replace('|^f^|','', $c) . ($op ? '^|p`t|^' : '');
}

?>
<style>
    * {
        font-size: 15px;
    }

    table {
        border-collapse: collapse;
    }
    #msg {
        border: 1px solid gray;
        padding: 10px;
        margin: 10px;
    }
    td {
        padding: 5px;
    }
</style>

<div id="msg"></div>
<table border="1" id="tbl">
    <!-- <?php foreach (getSubtitute($p) as $k => $v) { ?>
        <tr>
            <?php foreach ($v as $k1 => $v1) { ?>
                <td><?= $v1; ?></td>
            <?php } ?>
        </tr>
    <?php } ?> -->
    <tr><td></td></tr>
</table>

<script>
    $s = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-!@$%;&*( ),.|[]{}:';
    function getSubstitute($key){
        $sK = '';
        ($key+$s).split('').forEach(function (e) {
            if($sK.indexOf(e) == -1){
                $sK += e;
            }
        });
        return array_chunk($sK.split(''), 9);
    }
    function array_chunk(arr, len) {
        var chunks = [], i = 0, n = arr.length;
        while (i < n) {
            chunks.push(arr.slice(i, i += len));
        }
        return chunks;
    }
    function getOperated(...a){
        $h = [];
        $p = [];
        a[2].forEach(function(v, k){
            if(v.includes(a[1][0])){
                $h.push([k, v.indexOf(a[1][0])]);
            }
        });
        a[2].forEach(function(v, k){
            if(v.includes(a[1][1])){
                $h.push([k, v.indexOf(a[1][1])]);
            }
        });
        $p.push(
            a[3] ? 
                $h[0][0] == $h[1][0] ? 
                    [$h[0][1] == 8 ? a[2][$h[0][0]][0] : a[2][$h[0][0]][$h[0][1] + 1], $h[1][1] == 8 ? a[2][$h[0][0]][0] : a[2][$h[0][0]][$h[1][1] + 1]].join('') 
                :
                $h[0][1] == $h[1][1] ?
                    [a[2][$h[0][0] != 8 ? ($h[0][0] + 1) : 0][$h[0][1]],  a[2][$h[1][0] != 8 ? ($h[1][0] + 1) : 0][$h[1][1]]].join('')
                : 
                [a[2][$h[1][0]][$h[0][1]], a[2][$h[0][0]][$h[1][1]]].join('')
            :
                $h[0][0] == $h[1][0] ?
                    [$h[0][1] == 0 ? a[2][$h[0][0]][8] : a[2][$h[0][0]][$h[0][1] - 1], $h[1][1] == 0 ? a[2][$h[0][0]][8] : a[2][$h[0][0]][$h[1][1] - 1]].join('')
                :
                $h[0][1] == $h[1][1] ?
                    [a[2][$h[0][0] == 0 ? 8 : ($h[0][0] - 1)][$h[0][1]], a[2][$h[1][0] == 0 ? 8 : ($h[1][0] - 1)][$h[1][1]]].join('')
                :
                [a[2][$h[1][0]][$h[0][1]], a[2][$h[0][0]][$h[1][1]]].join('')
        );
        return $p;
    }
    function e($st, $k){
        $op = true;
        if($st.includes('^|p`t|^')){
            $st = $st.split('^|p`t|^')[0];
            $op = false;
        }
        $c = '';
        if($st.length%2){
            $st += '|^f^|';
        }
        $st = array_chunk($st.split(''), 2);
        $st.forEach(function($ch){
            if(!$s.includes($ch[0]) || !$s.includes($ch[1])){
                $c += $ch.join('');
            }else{
                $c += getOperated(1,$ch, getSubstitute($k), $op).join('');
            }
        });
        return $c.replace('|^f^|', '') + ($op ? '^|p`t|^' : '');
    }
    var t = 'Emmet is great for that. With it installed in the code editor you are using, you can type "lorem" and then tab and it will expand into a paragraph of Lorem Ipsum placeholder text. But it can do more! You can control how much you get, place it within [HTML] structure as it expands, and get different bits of it in repeated elements.';
    var p = 'A Quick Brown Fox Jumps Over The Lazy Dog';
    var txt = e(t, p);
    msg.innerHTML = t;
    msg.innerHTML += ' ---- ' + p;
    msg.innerHTML += '<br>';
    msg.innerHTML += txt
    msg.innerHTML += '<br>';
    msg.innerHTML += e(txt, p);
    
    // getSubstitute(p).forEach(function(e, i) {
    //     $tr = document.createElement('TR');
    //     e.forEach(function(t, x) {
    //         $td = document.createElement('TD');
    //         $td.innerHTML = t;
    //         $tr.appendChild($td);
    //     });
    //     tbl.appendChild($tr);
    // });
    </script>