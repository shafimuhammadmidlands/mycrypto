<style>
* {font-size: 15px;}
table {border-collapse: collapse;}
div {border: 1px solid gray;padding: 10px;margin: 10px;}
td {padding: 5px;}
li{padding: 10px;}
li:nth-child(odd) {color:#ffb122;}
li:nth-child(even) {color:#83de83;}
.abc {margin:0;padding:0; list-style:none;}
ul.abc > li:nth-child(odd) {color: #678dd2;}
ul.abc > li:nth-child(even) {color:#f2aec4;}
</style>
<div>
    <?php require_once('e.php');
        $pt = 'Emmet is great for that. With it installed in the code editor you are using, you can type "lorem" and then tab and it will expand into a paragraph of Lorem Ipsum placeholder text. But it can do more! You can control how much you get, place it within HTML structure as it expands, and get different bits of it in repeated elements.';
        $keyy = 'A Quick Brown Fox Jumps Over The Lazy Dog';
        echo '<ul><li>' . $pt . '</li><li>' . $keyy . '</li></ul>';
        $enc = e($pt, $keyy);
        echo '<ul class="abc"><li>' . $enc . '</li><li>' . e($enc, $keyy) . '</li></ul>';
    ?>
</div>
<div id="msg"></div>
<script src="e.php?e"></script>
<script>
var t = 'Emmet is great for that. With it installed in the code editor you are using, you can type "lorem" and then tab and it will expand into a paragraph of Lorem Ipsum placeholder text. But it can do more! You can control how much you get, place it within HTML structure as it expands, and get different bits of it in repeated elements.';
var p = 'A Quick Brown Fox Jumps Over The Lazy Dog';
var txt = e(t, p);
msg.innerHTML = '<ul><li>' + t + '</li><li>' + p + '</li></ul>';
msg.innerHTML += '<ul class="abc"><li>' + txt + '</li><li>' +  e(txt, p) + '</li></ul>';
</script>