var e=function(...a){
    switch(a[0]){
        default:
            return e(8,a[0],a[1]);
        case 1:
            $mk="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-!@$%;&*( ),.|[]{}:`^~/<>?_+=";
            return a[1]?$mk.substr(0,$mk.length-10):$mk;
        case 2:
            return e(1).split('');
        case 3:
            $s=e(1);
            $st=[];
            $s.split('').forEach(function(v,k){
                if(k!=0){
                    $t1=$s[0];
                    $s[0]='';
                    $s=$s.substring(1)+$t1;
                }
                $st[k]=$s.split('');
            });
            return $st;
        case 4:
            return e(2).indexOf(a[2])==-1||e(2).indexOf(a[1])==-1?a[1]:e(3)[e(2).indexOf(a[2])][e(2).indexOf(a[1])];
        case 5:
            return e(2).indexOf(a[2])==-1||e(2).indexOf(a[1])==-1?a[1]:e(2)[e(3)[e(2).indexOf(a[2])].indexOf(a[1])];
        case 6:
            $f=0;
            $d={
                'p':a[1],
                'k':a[2]
            };
            if(a[1].includes('^|p`t|^')){
                $d['p']=a[1].split('^|p`t|^')[0];
            }
            $ss=$d['p'].length;
            $ks=$d['k'].length;
            if($ss>$ks){
                $ni=$ss/$ks;
                $p=$d['k'];
                $f=1;
            }else if($ss<$ks){
                $ni=$ks/$ss;
                $p=$d['p']+'^|x`t|^';
                $f=2;
            }
            $sb='';
            if($ni){
                for($i=0;$i<$ni;$i++){
                    $sb+=$p;
                }
            }
            if($f==1){
                $d['k']=$sb.substr(0,$ss);
            }else if($f==2){
                $d['p']=$sb.substr(0,$ks);
            }
            $t='';
            if(!a[1].includes('^|p`t|^')){
                $d['p'].split('').forEach(function(v,k){
                    $t+=e(4,v,$d['k'].charAt(k));
                });
                return $t+'^|p`t|^';
            }
            $d['p'].split('').forEach(function(v,k){
                $t+=e(5,v,$d['k'].charAt(k));
            });
            return $t.split('^|x`t|^')[0];
        case 7:
            var c=[],i=0,n=a[1].length;
            while(i<n){
                c.push(a[1].slice(i,i+=a[2]));
            }
            return c;
        case 8:
            $sK='';
            (a[2]+e(1,1)).split('').forEach(function(v){
                if($sK.indexOf(v)==-1){
                    $sK+=v;
                }
            });
            return e(7,$sK.split(''),9);
        case 9:
            $h = [];
            $p = [];
            a[2].forEach(function(v, k){
                if(v.includes(a[1][0])){
                    $h.push([k, v.indexOf(a[1][0])]);
                }
            });
            a[2].forEach(function(v, k){
                if(v.includes(a[1][1])){
                    $h.push([k, v.indexOf(a[1][1])]);
                }
            });
            if(a[3]){
                $p.push(
                    $h[0][0] == $h[1][0] ? 
                        [$h[0][1] == 8 ? a[2][$h[0][0]][0] : a[2][$h[0][0]][$h[0][1] + 1], $h[1][1] == 8 ? a[2][$h[0][0]][0] : a[2][$h[0][0]][$h[1][1] + 1]].join('') 
                    :
                    $h[0][1] == $h[1][1] ?
                        [a[2][$h[0][0] != 8 ? ($h[0][0] + 1) : 0][$h[0][1]],  a[2][$h[1][0] != 8 ? ($h[1][0] + 1) : 0][$h[1][1]]].join('')
                    : 
                    [a[2][$h[1][0]][$h[0][1]], a[2][$h[0][0]][$h[1][1]]].join('')    
                );
            }else{
                $p.push(
                    $h[0][0] == $h[1][0] ?
                        [$h[0][1] == 0 ? a[2][$h[0][0]][8] : a[2][$h[0][0]][$h[0][1] - 1], $h[1][1] == 0 ? a[2][$h[0][0]][8] : a[2][$h[0][0]][$h[1][1] - 1]].join('')
                    :
                    $h[0][1] == $h[1][1] ?
                        [a[2][$h[0][0] == 0 ? 8 : ($h[0][0] - 1)][$h[0][1]], a[2][$h[1][0] == 0 ? 8 : ($h[1][0] - 1)][$h[1][1]]].join('')
                    :
                    [a[2][$h[1][0]][$h[0][1]], a[2][$h[0][0]][$h[1][1]]].join('')
                );
            }
            return $p;
        case 10:
            return '';
    }
}