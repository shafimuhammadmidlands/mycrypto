<?php

$s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-!@$%;&*( ),.|[]{}:`^~/<>?_+=";
$o = str_split($s);
$r = array_flip($o);
$permutiveArray = $o;
$subtituteTable = [];
foreach (str_split($s) as $k => $v) {
    if ($k != 0) {
        $permutiveValue = array_shift($permutiveArray);
        $permutiveArray[] = $permutiveValue;
    }
    $subtituteTable[] = $permutiveArray;
}

function enc($s, $k)
{
    global $r,$o, $subtituteTable;
     
    return !in_array($k, $o) || !in_array($s, $o)  ? $s : $subtituteTable[array_search($k, $o)][array_search($s, $o)];
    // return !isset($r[$k]) || !isset($r[$s]) ? $s : $subtituteTable[$r[$k]][$r[$s]];
}
function dec($s, $k)
{
    global $o, $r, $subtituteTable;
    return  !in_array($k, $o) || !in_array($s, $o) ? $s : $o[array_search($s, $subtituteTable[array_search($k, $o)])];
    // return !isset($r[$k]) || !isset($r[$s]) ? $s : $o[array_flip($subtituteTable[$r[$k]])[$s]];
}

function e($str, $key)
{
    $flag = 0;
    $data = [
        'pt' => $str,
        'key' => $key
    ];
    if (strpos($str, '^|p`t|^')) {
        $data['pt'] = explode('^|p`t|^', $str)[0];
    }
    $str_s = mb_strlen($data['pt']);
    $key_s = mb_strlen($data['key']);
    if ($str_s > $key_s) {
        $ni = $str_s / $key_s;
        $pre_permutive = $data['key'];
        $flag = 1;
    } else if ($str_s < $key_s) {
        $ni = $key_s / $str_s;
        $pre_permutive = $data['pt'] . '^|x`t|^';
        $flag = 2;
    }
    $substitute = '';
    if (isset($ni)) {
        for ($i = 0; $i < $ni; $i++) {
            $substitute .= $pre_permutive;
        }
    }
    if ($flag == 1) {
        $data['key'] = substr($substitute, 0, $str_s);
    } else if ($flag == 2) {
        $data['pt'] = substr($substitute, 0, $key_s);;
    }
    $txt = '';
    if (!strpos($str, '^|p`t|^')) {
        foreach (str_split($data['pt']) as $k => $v) {
            $txt .= enc($data['pt'][$k], $data['key'][$k]);
        }
        return $txt . '^|p`t|^';
    } else {
        foreach (str_split($data['pt']) as $k => $v) {
            $txt .= dec($data['pt'][$k], $data['key'][$k]);
        }
        return explode('^|x`t|^',$txt)[0];
    }
}

$pt = 'Emmet is great for that. With it installed in the code editor you are using, you can type "lorem" and then tab and it will expand into a paragraph of Lorem Ipsum placeholder text. But it can do more! You can control how much you get, place it within HTML structure as it expands, and get different bits of it in repeated elements.';
$keyy = 'A Quick Brown Fox Jumps Over The Lazy Dog';
echo $pt . "<br>" . $keyy;
echo "<br>" . $enc = e($pt, $keyy);
echo "<br><br>" . e($enc, $keyy);

?>
<style>
    * {
        font-size: 15px;
    }

    table {
        border-collapse: collapse;
    }
    #msg {
        border: 1px solid gray;
        padding: 10px;
        margin: 10px;
    }
</style>

<?php $t = bin2hex('HELLO
') ?>
<div id="msg"></div>
<table border="1" id="tbl">
    <!-- <?php foreach ($subtituteTable as $k => $v) { ?>
        <tr>
            <?php foreach ($v as $k1 => $v1) { ?>
                <td><?= $v1; ?></td>
            <?php } ?>
        </tr>
    <?php } ?> -->
    <tr><td></td></tr>
</table>
    <!-- <textarea id='txxt'></textarea>
<button onclick="test()">go</button> -->
<script>


    $s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890-!@$%;&*( ),.|[]{}:`^~/<>?_+=";
    $o = $s.split('');
    $st1 = [];
    $s.split('').forEach(function(v, k) {
        if (k != 0) {
            $t1 = $s[0];
            $s[0] = '';
            $s = $s.substring(1) + $t1
        }
        $st1[k] = $s.split('');
    });
    function enc($s, $k) {
        return $o.indexOf($k) == -1 || $o.indexOf($s) == -1 ? $s : $st1[$o.indexOf($k)][$o.indexOf($s)];
    }

    function dec($s, $k) {
        return  $o.indexOf($k) == -1 || $o.indexOf($s) == -1 ? $s : $o[$st1[$o.indexOf($k)].indexOf($s)];
    }

    function e($str, $key) {
        $f = 0;
        $d = {
            'p' : $str,
            'k' : $key
        };
        if ($str.includes('^|p`t|^')) {
            $d['p'] = $str.split('^|p`t|^')[0];
        }
        $ss = $d['p'].length;
        $ks = $d['k'].length;

        if ($ss > $ks) {
            $ni = $ss / $ks;
            $p = $d['k'];
            $f = 1;
        } else if ($ss < $ks) {
            $ni = $ks / $ss;
            $p = $d['p'] + '^|x`t|^';
            $f = 2;
        }
        $sb = '';
        if ($ni) {
            for ($i = 0; $i < $ni; $i++) {
                $sb += $p;
            }
        }
        if ($f == 1) {
            $d['k'] = $sb.substr(0, $ss);
        } else if ($f == 2) {
            $d['p'] = $sb.substr(0, $ks);
        }
        $t = '';
        if (!$str.includes('^|p`t|^')) {
            $d['p'].split('').forEach(function(e, k) {
                $t += enc(e, $d['k'].charAt(k));
            });
            return $t + '^|p`t|^';
        } else {
            $d['p'].split('').forEach(function(e, k) {
                $t += dec(e, $d['k'].charAt(k));
            });
            return $t.split('^|x`t|^')[0];
        }
        
    }



    var t = 'Emmet is great for that. With it installed in the code editor you are using, you can type "lorem" and then tab and it will expand into a paragraph of Lorem Ipsum placeholder text. But it can do more! You can control how much you get, place it within HTML structure as it expands, and get different bits of it in repeated elements.';
    var p = 'A Quick Brown Fox Jumps Over The Lazy Dog';
    var txt = e(t, p);
    msg.innerHTML = t;
    msg.innerHTML += ' ---- ' + p;
    msg.innerHTML += '<br>';
    msg.innerHTML += txt
    msg.innerHTML += '<br>';
    msg.innerHTML += e(txt, p);

    
    // function getHex($str){
    //     $str1 = '';
    //     $str.split('').forEach(function(e){
    //         var h = e.charCodeAt(0).toString(16);
    //         if(h.length == 1){
    //             h = '0' + h;
    //         }
    //         $str1 += h;
    //     });
    //     return $str1;
    // }
    // function getString(str1){
    //     var hx  = str1.toString();
    //     var str = '';
    //     for (var n = 0; n < hx.length; n += 2) {
    //         str += String.fromCharCode(parseInt(hx.substr(n, 2), 16));
    //     }
    //     return str;
    // }
    // $st1.forEach(function(e, i) {
    //     $tr = document.createElement('TR');
    //     e.forEach(function(t, x) {
    //         $td = document.createElement('TD');
    //         $td.innerHTML = t;
    //         $tr.appendChild($td);
    //     });
    //     tbl.appendChild($tr);
    // });
</script>